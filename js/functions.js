

$(function() {
	$(document).on('focusin', '.field, textarea', function() {
		if(this.title==this.value) {
			this.value = '';
		}
	}).on('focusout', '.field, textarea', function(){
		if(this.value=='') {
			this.value = this.title;
		}
	});
    /*-------------------- EXPANDABLE PANELS ----------------------*/
    var panelspeed = 500; //panel animate speed in milliseconds
    var totalpanels = 100; //total number of collapsible panels   
    var defaultopenpanel = -1; //leave -1 for no panel open   
	var defaultopenparentpanel = -1; //leave -1 for no panel open   
    var accordian = false; //set panels to behave like an accordian, with one panel only ever open at once      

    var panelheight = new Array();
	var panelparentheight = new Array();
    var currentpanel = defaultopenpanel;
	var currentparentpanel = defaultopenparentpanel;
    var iconheight = parseInt($('.icon-close-open').css('height'));

    //Initialise collapsible panels
    function panelinit() {


	
		for (var i = 0; i <= 6; i++) {
            panelparentheight[i] = parseInt($('#pp-' + i).find('.expandable-parpanel-content').css('height'));
			$('#pp-' + i).find('.expandable-parpanel-content').css('margin-top', -panelparentheight[i]);
		}
        for (var i = 0; i <= totalpanels; i++) {
            panelheight[i] = parseInt($('#cp-' + i).find('.expandable-panel-content').css('height'));
            $('#cp-' + i).find('.expandable-panel-content').css('margin-top', -panelheight[i]);
            if (defaultopenpanel == i) {
                $('#cp-' + i).find('.icon-close-open').css('background-position', '0px -' + iconheight + 'px');
                $('#cp-' + i).find('.expandable-panel-content').css('margin-top', 0);
            }
        }
		
		$('.expandable-parpanel-heading').css('zIndex', 1);
    }


    $('.expandable-panel-heading').click(function () {
        var obj = $(this).next();
        var objid = parseInt($(this).parent().attr('ID').substr(3, 2));		
        currentpanel = objid;
        if (accordian == true) {
            resetpanels();
        }

        if (parseInt(obj.css('margin-top')) <= (panelheight[objid] * -1)) {
            obj.clearQueue();
            obj.stop();
            obj.prev().find('.icon-close-open').css('background-position', '0px -' + iconheight + 'px');
            obj.animate({
                'margin-top': 0
            }, panelspeed);
        } else {
            obj.clearQueue();
            obj.stop();
            obj.prev().find('.icon-close-open').css('background-position', '0px 0px');
            obj.animate({
                'margin-top': (panelheight[objid] * -1)
            }, panelspeed);
        }
    });

	$('.expandable-parpanel-heading').click(function () {		
		$(this).css('zIndex', 2);
        var obj = $(this).next();
        var objid = parseInt($(this).parent().attr('ID').substr(3, 2));
		var children = parseInt($(this).parent().attr('CHILDREN'));		
		currentparentpanel = objid;
        if (accordian == true) {
            resetpanels();
        }

        if (parseInt(obj.css('margin-top')) <= (panelparentheight[objid] * -1)) {
            obj.clearQueue();
            obj.stop();
            obj.prev().find('.icon-close-open').css('background-position', '0px -' + iconheight + 'px');
            obj.animate({
                'margin-top': 0
            }, panelspeed);
        } else {
            obj.clearQueue();
            obj.stop();
            obj.prev().find('.icon-close-open').css('background-position', '0px 0px');
            obj.animate({
                'margin-top': ((panelheight[objid] * 1.4 * (-1 *(children)))  + (children * 20))
            }, panelspeed);
        }
    });
	
    function resetpanels() {
        for (var i = 1; i <= totalpanels; i++) {
            if (currentpanel != i) {
                $('#cp-' + i).find('.icon-close-open').css('background-position', '0px 0px');
                $('#cp-' + i).find('.expandable-panel-content').animate({'margin-top': -panelheight[i]}, panelspeed);			
            }
        }
		for (var i = 1; i <= 6; i++) {
			if (currentparentpanel != i) {				
				$('#pp-' + i).find('.icon-close-open').css('background-position', '0px 0px');
				$('#pp-' + i).find('.expandable-parpanel-content').animate({'margin-top': -panelparentheight[i]}, panelspeed);
			}
		}
    }
    
   // run once window has loaded    
   panelinit();

});

$(window).load(function() {	

	$('.flexslider').flexslider({
		animation: "slide",
		controlsContainer: ".slider-holder",
		slideshowSpeed: 5000,
		directionNav: true,
		controlNav: false,
		animationDuration: 900
	});
	
}
);